<?php

namespace Modules\Reserve\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RestrictionSetting extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table ='restriction_setting';
    protected $primaryKey = 'id';


    protected static function newFactory()
    {
        return \Modules\Reserve\Database\factories\RestrictionSettingFactory::new();
    }
}

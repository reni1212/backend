<?php

namespace Modules\Reserve\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Reservations extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'reservation_timestamp_utc'];
    protected $table ='reservations';
    protected $primaryKey = 'id';

    
    protected static function newFactory()
    {
        return \Modules\Reserve\Database\factories\ReservationsFactory::new();
    }
}

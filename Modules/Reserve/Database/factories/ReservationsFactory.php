<?php
namespace Modules\Reserve\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ReservationsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Reserve\Entities\Reservations::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}


<?php

namespace Modules\Reserve\Http\Services;


use Modules\Reserve\Models\RestrictionSetting;
use Modules\Reserve\Models\Reservations;

class ReserveCommonService
{
    public function getReservationSetting(){
        return RestrictionSetting::select('g')->where('id','1')->first();
    }

    public function checkUserInfoInd($userId, $dateTimeStamp){
        return Reservations::select('user_id')->where('reservation_timestamp_utc', '=',$dateTimeStamp)->where('user_id','=',$userId)->get()->toArray();
    }

    public function checkUserInfoGrp($userIdArr, $dateTimeStamp){
        return Reservations::select('user_id')->where('reservation_timestamp_utc', '=', $dateTimeStamp)->wherein('user_id', $userIdArr)->get()->toArray();
      
    }
}

<?php

namespace Modules\Reserve\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Reserve\Http\Services\ReserveCommonService;

class ReserveController extends Controller
{
    public function __construct(ReserveCommonService $reserveCommonService)
    {
        $this->reserveService = $reserveCommonService;
    }

    public function addReservation(Request $request){

        $reserveSetting = $this->reserveService->getReservationSetting();
        $userIdArr = explode(',', $request->data['user_ids'][0]);
        $dateTimeStamp = strtotime($request->data['reservation_datetime'][0]);
        $resDef['is_booking_restricted'] = false;
        if($reserveSetting->g == 'individual'){
            $resIndVal = $this->getIndividualValidation($userIdArr, $dateTimeStamp);
            if(!empty($resIndVal)){
                $res = $this->setRestrictedUserArr($resIndVal);
            }else{
                $res = $resDef;
            }
        }elseif($reserveSetting->g == 'group'){
            $resGrpVal = $this->getGroupValidation($userIdArr,$dateTimeStamp );
            if(!empty($resGrpVal)){
                $res = $this->setRestrictedUserArr($resGrpVal);
            }else{
                $res = $resDef;
            }
        }
        return response()->json(['data'=>$res], 200);
    }

    private function getIndividualValidation($userData, $dateTimeStamp ){

        $resUsrIdArr = [];
        foreach($userData as $value){
            $resInfo = $this->reserveService->checkUserInfoInd($value,$dateTimeStamp);
            if(!empty($resInfo)){
                $resUsrIdArr[] = $value;
            }
        }
        return $resUsrIdArr;
    }

    private function getGroupValidation($userData, $dateTimeStamp){
        
        $resUsrIdArr = [];
        $resInfo = $this->reserveService->checkUserInfoGrp($userData,$dateTimeStamp);
        if(!empty($resInfo)){
            $resUsrIdArr = array_column($resInfo, 'user_id');
        }
        return $resUsrIdArr;
    }

    private function setRestrictedUserArr($userArr){

        $result['is_booking_restricted'] = true;
        $result['restricted_user_ids'] = $userArr;
        return $result;
    }


}
